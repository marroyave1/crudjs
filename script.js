var default_content = "";

var listInfo = [
	{
		"id": 1,
		"nombre": "Emp1",
		"jefe": {
			"id": 1,
			"nombre": "Lenin"
		}
	},
	{
		"id": 2,
		"nombre": "Emp2",
		"jefe": {
			"id": 2,
			"nombre": "Marx"
		}
	}
];
var isCreating = false;
var isEditing = false;

$(document).ready(function () {
	buildTable();
	$('#btnNuevo').click(function (e) {
		showCru();
	});
	$('#btnAgregar').click(function (e) {
		create();
	});
	$('#btnEditar').click(function (e) {
		edit();
	});


});

function edit() {
	var existingId = $('#idL').val();
	var newName = $('#nameL').val();
	var newJefe = $('#jefeL').val();

	var llTo = [];
	itemToEdit = listInfo.find(
		(itemTo) => {
			return itemTo.id === Number(existingId)
		}
	)
	switch (Number(newJefe)) {
		case 1: listInfo[listInfo.indexOf(itemToEdit)] = {
			'id': Number(existingId),
			'nombre': newName,
			'jefe': {
				'id': Number(newJefe),
				'nombre': 'JJ'
			}
		}
			break;
		case 2: listInfo[listInfo.indexOf(itemToEdit)] = {
			'id': Number(existingId),
			'nombre': newName,
			'jefe': {
				'id': Number(newJefe),
				'nombre': 'HH'
			}
		}
			break;
		case 3:
			listInfo[listInfo.indexOf(itemToEdit)] = {
				'id': Number(existingId),
				'nombre': newName,
				'jefe': {
					'id': Number(newJefe),
					'nombre': 'MM'
				}
			}
			break;
	}


	listInfo[listInfo.indexOf(itemToEdit)] = {
		'id': Number(existingId),
		'nombre': newName,
		'jefe': {
			'id': Number(newJefe),
			'nombre': 'MM'
		}
	}

	buildTable();

}

function create() {
	debugger;
	var newName = $('#nameL').val();
	var newJefe = $('#jefeL').children("option:selected").val();
	var newId = $('#idL').val();

	var llTo = [];
	switch (Number(newJefe)) {
		case 1: listInfo.push({
			'id': newId,
			'nombre': newName,
			'jefe': {
				'id': Number(newJefe),
				'nombre': 'JJ'
			}
		});
			break;
		case 2: listInfo.push({
			'id': newId,
			'nombre': newName,
			'jefe': {
				'id': Number(newJefe),
				'nombre': 'HH'
			}
		});
			break;
		case 3: listInfo.push({
			'id': newId,
			'nombre': newName,
			'jefe': {
				'id': Number(newJefe),
				'nombre': 'MM'
			}
		});
			break;
	}
	buildTable();
	showCru();
}

function deleteItem(item) {
	itemToDelete = listInfo.find(
		(itemTo) => {
			return itemTo.id === item
		}
	)

	listInfo.splice(listInfo.indexOf(itemToDelete), 1);

	buildTable();
}

function viewItem(item) {
	itemToView = listInfo.find(
		(itemTo) => {
			return itemTo.id === item
		}
	)

	$('#cruMain').css('display', 'block');
	$('#btnEditar').css('display', 'block');
	$('#idL').css('display', 'block');
	$('#btnAgregar').css('display', 'none');

	$('#idL').val(itemToView.id);
	$('#nameL').val(itemToView.nombre);
	$('#jefeL').val(itemToView.jefe.id);
}

function showCru() {
	$('#cruMain').css('display', 'block');
	$('#idL').css('display', 'block');
	$('#btnEditar').css('display', 'none');
	$('#btnAgregar').css('display', 'block');
	$('#idL').val('');
	$('#nameL').val('');
	$('#jefeL').val('');
}

function buildTable() {
	//$('#tableInfo').html(msg);
	var table = '';
	table += '<tr><th>Id</th><th>Nombre</th><th>Jefe Directo</th><th>Opciones</th></tr>'

	listInfo.forEach(
		(listItem) => {
			table += '<tr>' + '<td>' + listItem.id + '</td>' + '<td>'
				+ listItem.nombre + '</td>' + '<td>' + listItem.jefe.id + '</td>'
				+ '<td><button class="button button2" onclick="viewItem(' + listItem.id + ')">Ver</button><button class="button button2" onclick="deleteItem(' + listItem.id + ')">Eliminar</button></td>' + '</tr>'
		}
	)

	$('#tableInfo').html(table);

}
